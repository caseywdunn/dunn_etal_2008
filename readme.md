# Dunn et al. 2008

This repository contains files for the analyses presented in:

> Dunn, CW, A Hejnol, DQ Matus, K Pang, WE Browne, SA Smith, E Seaver, GW Rouse, M Obst, GD Edgecombe, MV Sorensen, SHD Haddock, A Schmidt-Rhaesa, A Okusu, RM Kristensen, WC Wheeler, MQ Martindale, and G Giribet (2008) Broad phylogenomic sampling improves resolution of the Animal Tree of Life. Nature. 452:745-749. [doi:10.1038/nature06614](http://dx.doi.org/10.1038/nature06614)

## Trees

The files are:

- `Figure1.tre` (source path: `protol/10May07build_runs/matrix_b25t/all/rax/04Jul07rax/raxml_bkt_consolidate.best`)

- `Figure2.tre` (source path: `protol/10May07build_runs/matrix_b25t/stab0.90/for\ figures/pbtopology.tre`)
